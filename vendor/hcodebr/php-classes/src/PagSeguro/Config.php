<?php
namespace HCode\PagSeguro;

class Config{
	//caso mude para false todas as urls de SandBox irão trocar para produção
	const SANDBOX = true;

	const SANDBOX_EMAIL = "EMAIL DO SADBOX PAGSEGURO";
    const PRODUCTION_EMAIL = "EMAIL DA INTEGRAÇÃO EM PRODUÇÃO";

	const SANDBOX_TOKEN = "TOKEN DO SANDBOX";
	const PRODUCTION_TOKEN = "TOKEN DA PRODUÇÃO";
	
	const SANDBOX_SESSIONS = "https://ws.sandbox.pagseguro.uol.com.br/v2/sessions";
    const PRODUCTION_SESSIONS = "https://ws.pagseguro.uol.com.br/v2/sessions";

    const SANDBOX_URL_JS = "https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js";
    const PRODUCTION_URL_JS = "https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js";

    const PRODUCTION_URL_TRANSACTION = "https://ws.pagseguro.uol.com.br/v2/transactions";
    const SANDBOX_URL_TRANSACTION = "https://ws.sandbox.pagseguro.uol.com.br/v2/transactions";

    const SANDBOX_URL_NOTIFICATION = "https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications/";
    const PRODUCTION_URL_NOTIFICATION = "https://ws.pagseguro.uol.com.br/v2/transactions/notifications/";

    //parcelas sem juros
    const MAX_INSTALLMENT_NO_INTEREST = 10;

    //parcelas com juros.
    const MAX_INSTALLMENT = 10;
    
    //após ter um dominio precisa trocar esse parametro para resceber as notificações do pagseguro
	const NOTIFICATION_URL = "https://alugueumcarro.000webhostapp.com/payment/notification";



	 public static function getAuthentication():array
    {

        if (Config::SANDBOX === true)
        {

            return [
                "email"=>Config::SANDBOX_EMAIL,
                "token"=>Config::SANDBOX_TOKEN
            ];

        } else {

            return [
                "email"=>Config::PRODUCTION_EMAIL,
                "token"=>Config::PRODUCTION_TOKEN
            ];

        }

    }

    public static function getUrlSessions():string
    {

        return (Config::SANDBOX === true) ? Config::SANDBOX_SESSIONS : Config::PRODUCTION_SESSIONS;

    }

    public static function getUrlJS()
    {

        return (Config::SANDBOX === true) ? Config::SANDBOX_URL_JS : Config::PRODUCTION_URL_JS;        

    }

    public static function getUrlTransaction()
    {

        return (Config::SANDBOX === true) ? Config::SANDBOX_URL_TRANSACTION : Config::PRODUCTION_URL_TRANSACTION;        

    }
    
    public static function getNotificationTransactionURL()
    {

        return (Config::SANDBOX === true) ? Config::SANDBOX_URL_NOTIFICATION : Config::PRODUCTION_URL_NOTIFICATION;        

    }

}

?>