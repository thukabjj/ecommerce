<?php 

namespace Hcode;

use Rain\Tpl;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer {

	const USERNAME = "COLOCAR USER NAME DO EMAIL";
	const PASSWORD = "SENHA DO EMAIL";
	const HOST = "HOST DO EMAIL";
	const PORT = 'PORTA DO EMAIL';
	const NAME_FROM = "NOME QUE IRÁ APARECER NO EMAIL";
	private $mail;

	public function __construct($toAddress,$toName,$subject,$tplName,$data = array())
	{
		utf8_encode($toAddress);
		utf8_encode($toName);
		utf8_encode($subject);
		$config = array(
			"base_url"      => null,
			"tpl_dir"       => $_SERVER['DOCUMENT_ROOT']."/views/email/",
			"cache_dir"     => $_SERVER['DOCUMENT_ROOT']."/views-cache/",
			"debug"         => false
			);

		Tpl::configure( $config );

		$tpl = new Tpl;

		foreach ($data as $key => $value) {
			$tpl->assign($key, $value);
		}

		$html = $tpl->draw($tplName, true);

		echo "<h1>".(extension_loaded('openssl')?'SSL loaded':'SSL not loaded')."</h1>"."\n";

		$this->mail = new PHPMailer();

		$this->mail->Charset = 'UTF-8';
		$this->mail->isHTML();
		$this->mail->SMTPSecure = 'tls';
		$this->mail->isSMTP();
		$this->mail->Host = Mailer::HOST;

		$this->mail->SMTPDebug = 2;
		$this->mail->Port = Mailer::PORT;
		$this->mail->SMTPAuth = false;
		$this->mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
				)
			);
		$this->mail->Username = Mailer::USERNAME;
		$this->mail->Password = Mailer::PASSWORD;

		$this->mail->setFrom(Mailer::USERNAME,Mailer::NAME_FROM);
		$this->mail->addAddress($toAddress,$toName);
		$this->mail->Subject = ($subject);
		$this->mail->msgHTML($html);
		$this->mail->AltBody ="Alternativo";

	}
	public function send()
	{

		return $this->mail->send();
	}
}

?>