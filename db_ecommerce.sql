/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.19 : Database - db_ecommerce
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_ecommerce` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_ecommerce`;

/*Table structure for table `tb_addresses` */

DROP TABLE IF EXISTS `tb_addresses`;

CREATE TABLE `tb_addresses` (
  `idaddress` int(11) NOT NULL AUTO_INCREMENT,
  `idperson` int(11) NOT NULL,
  `desaddress` varchar(128) NOT NULL,
  `desnumber` varchar(16) NOT NULL,
  `descomplement` varchar(32) DEFAULT NULL,
  `descity` varchar(32) NOT NULL,
  `desstate` varchar(32) NOT NULL,
  `descountry` varchar(32) NOT NULL,
  `deszipcode` char(8) NOT NULL,
  `desdistrict` varchar(32) NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idaddress`),
  KEY `fk_addresses_persons_idx` (`idperson`),
  CONSTRAINT `fk_addresses_persons` FOREIGN KEY (`idperson`) REFERENCES `tb_persons` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

/*Data for the table `tb_addresses` */

insert  into `tb_addresses`(`idaddress`,`idperson`,`desaddress`,`desnumber`,`descomplement`,`descity`,`desstate`,`descountry`,`deszipcode`,`desdistrict`,`dtregister`) values (1,1,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-10-11 16:12:26'),(2,10,'Rua Lauro Bento','522','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 13:57:28'),(3,10,'Rua Lauro Bento','20','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 14:00:37'),(4,10,'Rua Lauro Bento','20','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 16:04:40'),(5,10,'Rua Lauro Bento','20','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 16:13:16'),(6,10,'Rua Lauro Bento','20','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 16:18:29'),(7,10,'Rua Lauro Bento','200','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 16:19:04'),(8,10,'Rua Lauro Bento','20','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 16:22:14'),(9,10,'Rua Lauro Bento','20','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 16:25:28'),(10,10,'Rua Lauro Bento','20','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 16:43:20'),(11,10,'Rua Lauro Bento','20','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 16:45:14'),(12,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-21 16:47:08'),(13,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-21 16:49:01'),(14,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-21 16:57:23'),(15,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-21 16:58:12'),(16,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-21 16:59:04'),(17,10,'Rua Lauro Bento','23','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 17:07:33'),(18,10,'Rua Lauro Bento','52','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 17:10:14'),(19,10,'Rua Lauro Bento','54','','São Paulo','SP','Brasil','02473080','Vila Roque','2017-11-21 17:23:56'),(20,10,'rua cambuci','20','','dsahdouasho','asdhsa','brasil','06813090','asiudgsaiu','2017-11-21 20:46:14'),(21,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-22 08:18:32'),(22,10,'Rua Cambuci','34','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-22 11:23:24'),(23,10,'Rua Cambuci','563','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-22 11:25:58'),(24,10,'Rua Cambuci','563','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-22 11:36:03'),(25,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-24 10:32:38'),(26,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-24 10:49:08'),(27,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-24 11:58:12'),(28,10,'Rua Cambuci','23','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-24 12:15:35'),(29,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-27 12:11:08'),(30,10,'Rua Cambuci','10','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-27 12:11:26'),(31,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-27 12:44:54'),(32,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-27 12:52:46'),(33,10,'Rua Cambuci','12','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-29 15:15:53'),(34,10,'Rua Cambuci','32','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-29 16:38:04'),(35,10,'Rua Cambuci','56','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-29 16:41:43'),(36,10,'Rua Cambuci','556','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-11-29 16:57:50'),(37,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-01 15:08:56'),(38,10,'Rua Cambuci','324','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-04 10:28:17'),(39,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-06 08:35:10'),(40,1,'Rua Cambuci','12','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-06 12:54:23'),(41,1,'Rua Cambuci','23','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-06 13:01:38'),(42,10,'Rua Cerqueira César','23','','Embu das Artes','SP','Brasil','06813000','Jardim Santa Tereza','2017-12-06 16:03:05'),(43,10,'Rua Cerqueira César','23','','Embu das Artes','SP','Brasil','06813000','Jardim Santa Tereza','2017-12-06 16:25:24'),(44,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-06 17:19:25'),(45,10,'Rua Cambuci','23','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-08 11:36:11'),(46,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-08 12:46:08'),(47,10,'Rua Cambuci','22','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-12 11:40:53'),(48,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-12 11:54:20'),(49,10,'Rua Cambuci','23','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-12 15:28:33'),(50,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-12 15:31:10'),(51,10,'Rua Cambuci','20','','Embu das Artes','SP','Brasil','06813090','Jardim Santa Tereza','2017-12-13 16:41:41');

/*Table structure for table `tb_carts` */

DROP TABLE IF EXISTS `tb_carts`;

CREATE TABLE `tb_carts` (
  `idcart` int(11) NOT NULL AUTO_INCREMENT,
  `dessessionid` varchar(64) NOT NULL,
  `iduser` int(11) DEFAULT NULL,
  `deszipcode` char(8) DEFAULT NULL,
  `vlfreight` decimal(10,2) DEFAULT NULL,
  `nrdays` int(11) DEFAULT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcart`),
  KEY `FK_carts_users_idx` (`iduser`),
  CONSTRAINT `fk_carts_users` FOREIGN KEY (`iduser`) REFERENCES `tb_users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

/*Data for the table `tb_carts` */

insert  into `tb_carts`(`idcart`,`dessessionid`,`iduser`,`deszipcode`,`vlfreight`,`nrdays`,`dtregister`) values (4,'tudub0duatg92c4jvpnulvkeu0',1,'06813090','61.82',1,'2017-09-27 09:42:51'),(5,'vh9vq2dmuu1or2umpsmhaqvpf0',1,NULL,NULL,NULL,'2017-10-02 15:57:15'),(6,'s9uta8dmn1tn411l9mj6j93r30',NULL,'06813090','61.82',1,'2017-10-03 11:10:07'),(7,'patup9gsr671bv954181s1lmv4',NULL,'06813090','96.08',1,'2017-10-03 16:49:32'),(8,'h3brf5ncdo98p36ij9jqqjma64',1,'06813090','43.49',1,'2017-10-04 12:59:36'),(9,'5molvuaio651rsqm1mrjrnfld1',1,'04910070','61.82',1,'2017-10-06 15:08:18'),(10,'n3s3esrv8ivkd818g3s7fc55d6',NULL,'06813090','150.25',1,'2017-10-09 10:04:01'),(11,'sk67t9n2ovgu15nofa6uro44h3',1,NULL,NULL,NULL,'2017-10-11 11:01:48'),(12,'0g5in363q9gi1gk8f9rm300c91',1,'06813090','36.45',1,'2017-10-11 16:11:25'),(13,'ghofg65unu54a45lqja4ejc9f5',NULL,NULL,NULL,NULL,'2017-10-23 12:19:25'),(14,'h2smm9galqeiscbdbjludlsc01',NULL,'02473080','45.74',1,'2017-11-21 12:51:27'),(15,'5hcopsthd7d7e4o55is1cpkkm8',NULL,'06813090','43.49',1,'2017-11-21 16:46:42'),(16,'1vtlcjhu1gtakrg7lrliultcfl',NULL,'06813090',NULL,NULL,'2017-11-21 16:58:37'),(17,'adud0soco1nna198op6hefs04n',NULL,'',NULL,NULL,'2017-11-21 20:42:43'),(18,'i789og4hsjtj5dcovtd1t9n5ns',NULL,'06813090','43.49',1,'2017-11-22 08:17:15'),(19,'ldlim9tlvlmu5rdm38f2mpokgf',NULL,'06813090','61.82',3,'2017-11-24 10:31:35'),(20,'e1ktkmp2sv8l65g72f25g150pi',NULL,'06813090','43.49',3,'2017-11-27 12:10:28'),(21,'682115tbbs6qh6o0td34v8i6v7',NULL,'06813090','43.49',3,'2017-11-29 15:15:16'),(22,'iecnvfgjong536jrkoqkec5h4k',NULL,'06813090','43.49',3,'2017-12-01 15:07:58'),(23,'cljhj1jjksejnoagnf553965l4',NULL,'06813090','43.49',3,'2017-12-04 10:27:06'),(24,'vvrm186ijpn1jpda1qjefrae3l',NULL,'06813090','43.49',3,'2017-12-06 08:34:29'),(25,'l0u5stbid4hbltkv1a878urte6',NULL,'06813090','71.80',3,'2017-12-06 15:59:30'),(26,'aj8k60v15g4rl7u343vi7aas1n',NULL,'06813090','43.49',3,'2017-12-08 11:35:22'),(27,'1b9mukumhgi7qv5spfkd3qss9e',NULL,'06813090','43.49',3,'2017-12-12 11:40:00'),(28,'iqq6gkc0mr3surv98ep433d1pb',NULL,'06813090','43.49',3,'2017-12-13 16:40:49');

/*Table structure for table `tb_cartsproducts` */

DROP TABLE IF EXISTS `tb_cartsproducts`;

CREATE TABLE `tb_cartsproducts` (
  `idcartproduct` int(11) NOT NULL AUTO_INCREMENT,
  `idcart` int(11) NOT NULL,
  `idproduct` int(11) NOT NULL,
  `dtremoved` datetime DEFAULT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcartproduct`),
  KEY `FK_cartsproducts_carts_idx` (`idcart`),
  KEY `FK_cartsproducts_products_idx` (`idproduct`),
  CONSTRAINT `tb_cartsproducts_ibfk_1` FOREIGN KEY (`idcart`) REFERENCES `tb_carts` (`idcart`),
  CONSTRAINT `tb_cartsproducts_ibfk_2` FOREIGN KEY (`idproduct`) REFERENCES `tb_products` (`idproduct`)
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8;

/*Data for the table `tb_cartsproducts` */

insert  into `tb_cartsproducts`(`idcartproduct`,`idcart`,`idproduct`,`dtremoved`,`dtregister`) values (1,3,1,'2017-09-26 15:32:48','2017-09-26 15:02:07'),(2,3,5,'2017-09-26 15:34:03','2017-09-26 15:23:20'),(3,3,1,'2017-09-26 15:34:06','2017-09-26 15:32:43'),(4,3,1,'2017-09-26 15:34:09','2017-09-26 15:33:58'),(5,3,5,'2017-09-26 15:34:23','2017-09-26 15:34:01'),(6,3,7,NULL,'2017-09-26 15:37:43'),(7,3,7,NULL,'2017-09-26 15:37:43'),(8,3,7,NULL,'2017-09-26 15:37:44'),(9,3,7,NULL,'2017-09-26 15:37:44'),(10,3,7,NULL,'2017-09-26 15:37:44'),(11,3,7,NULL,'2017-09-26 15:37:44'),(12,3,7,NULL,'2017-09-26 15:37:44'),(13,3,7,NULL,'2017-09-26 15:37:44'),(14,3,7,NULL,'2017-09-26 15:37:44'),(15,3,7,NULL,'2017-09-26 15:37:44'),(16,3,7,NULL,'2017-09-26 15:37:44'),(17,3,7,NULL,'2017-09-26 15:37:44'),(18,3,7,NULL,'2017-09-26 15:37:44'),(19,3,7,NULL,'2017-09-26 15:37:44'),(20,3,7,NULL,'2017-09-26 15:37:44'),(21,3,7,NULL,'2017-09-26 15:37:44'),(22,3,7,NULL,'2017-09-26 15:37:44'),(23,3,7,NULL,'2017-09-26 15:37:44'),(24,3,7,NULL,'2017-09-26 15:37:44'),(25,3,7,NULL,'2017-09-26 15:37:44'),(26,3,7,NULL,'2017-09-26 15:37:44'),(27,3,7,NULL,'2017-09-26 15:37:44'),(28,3,7,NULL,'2017-09-26 15:37:44'),(29,3,7,NULL,'2017-09-26 15:37:44'),(30,3,7,NULL,'2017-09-26 15:37:44'),(31,3,7,NULL,'2017-09-26 15:37:44'),(32,3,7,NULL,'2017-09-26 15:37:44'),(33,3,7,NULL,'2017-09-26 15:37:44'),(34,3,7,NULL,'2017-09-26 15:37:44'),(35,3,7,NULL,'2017-09-26 15:37:45'),(36,3,7,NULL,'2017-09-26 15:37:45'),(37,3,7,NULL,'2017-09-26 15:37:45'),(38,3,7,NULL,'2017-09-26 15:37:45'),(39,3,7,NULL,'2017-09-26 15:37:45'),(40,3,7,NULL,'2017-09-26 15:37:45'),(41,3,7,NULL,'2017-09-26 15:37:45'),(42,3,7,NULL,'2017-09-26 15:37:45'),(43,3,7,NULL,'2017-09-26 15:37:45'),(44,3,7,NULL,'2017-09-26 15:37:45'),(45,3,7,NULL,'2017-09-26 15:37:45'),(46,3,7,NULL,'2017-09-26 15:37:45'),(47,3,7,NULL,'2017-09-26 15:37:45'),(48,3,7,NULL,'2017-09-26 15:37:45'),(49,3,7,NULL,'2017-09-26 15:37:45'),(50,3,7,NULL,'2017-09-26 15:37:45'),(51,3,7,NULL,'2017-09-26 15:37:45'),(52,3,7,NULL,'2017-09-26 15:37:45'),(53,3,7,NULL,'2017-09-26 15:37:45'),(54,3,7,NULL,'2017-09-26 15:37:45'),(55,3,7,NULL,'2017-09-26 15:37:45'),(56,4,1,'2017-09-27 10:07:09','2017-09-27 10:07:05'),(57,4,5,'2017-09-27 11:25:52','2017-09-27 10:46:14'),(58,4,5,'2017-09-27 11:25:58','2017-09-27 10:46:14'),(59,4,5,'2017-09-27 11:27:09','2017-09-27 10:46:14'),(60,4,5,'2017-09-27 11:27:11','2017-09-27 10:46:15'),(61,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(62,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(63,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(64,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(65,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(66,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(67,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(68,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(69,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(70,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(71,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(72,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(73,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(74,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(75,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(76,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(77,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(78,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(79,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(80,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(81,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(82,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(83,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:15'),(84,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(85,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(86,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(87,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(88,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(89,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(90,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(91,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(92,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(93,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(94,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(95,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(96,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(97,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(98,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(99,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(100,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(101,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(102,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(103,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(104,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(105,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(106,4,5,'2017-09-27 11:27:14','2017-09-27 10:46:16'),(107,4,1,'2017-09-27 11:25:44','2017-09-27 10:46:32'),(108,4,1,'2017-09-27 11:25:44','2017-09-27 10:46:35'),(109,4,1,'2017-09-27 11:25:44','2017-09-27 10:46:40'),(110,4,1,'2017-09-27 11:25:44','2017-09-27 10:46:50'),(111,4,1,'2017-09-27 11:25:44','2017-09-27 10:47:01'),(112,4,1,'2017-09-27 11:25:44','2017-09-27 10:47:03'),(113,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:17'),(114,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:17'),(115,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:17'),(116,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:17'),(117,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(118,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(119,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(120,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(121,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(122,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(123,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(124,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(125,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(126,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(127,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(128,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(129,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(130,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(131,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(132,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(133,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(134,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(135,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(136,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(137,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(138,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(139,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(140,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(141,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(142,4,6,'2017-09-27 11:25:42','2017-09-27 10:47:18'),(143,4,3,'2017-09-27 15:24:21','2017-09-27 11:27:25'),(144,4,3,'2017-09-27 15:25:15','2017-09-27 11:27:25'),(145,4,3,'2017-09-27 15:25:19','2017-09-27 15:24:52'),(146,4,3,'2017-09-27 15:34:14','2017-09-27 15:25:09'),(147,4,3,'2017-09-27 15:34:14','2017-09-27 15:34:08'),(148,4,3,'2017-09-27 15:34:14','2017-09-27 15:34:11'),(149,4,5,'2017-09-27 15:48:33','2017-09-27 15:35:13'),(150,4,5,'2017-09-27 15:48:33','2017-09-27 15:35:16'),(151,4,5,'2017-09-27 15:48:33','2017-09-27 15:41:00'),(152,4,5,'2017-09-27 15:48:33','2017-09-27 15:48:09'),(153,4,5,'2017-09-27 15:48:33','2017-09-27 15:48:13'),(154,4,5,'2017-09-27 15:48:33','2017-09-27 15:48:18'),(155,4,5,'2017-09-27 15:48:33','2017-09-27 15:48:23'),(156,4,5,NULL,'2017-09-27 16:35:29'),(157,4,5,NULL,'2017-09-29 15:34:51'),(158,6,5,NULL,'2017-10-03 11:20:06'),(159,6,5,NULL,'2017-10-03 11:53:23'),(160,7,5,NULL,'2017-10-03 16:49:47'),(161,7,5,NULL,'2017-10-03 16:49:49'),(162,7,5,NULL,'2017-10-03 16:49:51'),(163,7,2,NULL,'2017-10-03 16:53:18'),(164,8,5,NULL,'2017-10-04 13:00:40'),(165,9,5,NULL,'2017-10-06 15:08:38'),(166,9,5,NULL,'2017-10-06 16:42:12'),(167,10,5,NULL,'2017-10-09 10:04:01'),(168,10,5,NULL,'2017-10-09 10:04:03'),(169,10,2,NULL,'2017-10-09 10:08:48'),(170,10,5,NULL,'2017-10-09 10:08:52'),(171,10,5,NULL,'2017-10-09 14:12:36'),(172,10,2,NULL,'2017-10-09 14:12:40'),(173,12,1,'2017-10-11 16:11:40','2017-10-11 16:11:30'),(174,12,6,NULL,'2017-10-11 16:11:57'),(175,14,5,'2017-11-21 16:18:41','2017-11-21 12:59:12'),(176,14,5,'2017-11-21 16:18:41','2017-11-21 16:03:52'),(177,14,1,'2017-11-21 16:18:45','2017-11-21 16:13:04'),(178,14,5,'2017-11-21 16:18:41','2017-11-21 16:18:38'),(179,14,4,NULL,'2017-11-21 16:18:51'),(180,14,1,'2017-11-21 16:22:00','2017-11-21 16:21:57'),(181,15,5,NULL,'2017-11-21 16:46:54'),(182,17,5,NULL,'2017-11-21 20:45:12'),(183,18,5,NULL,'2017-11-22 08:17:40'),(184,19,5,NULL,'2017-11-24 10:32:12'),(185,19,5,NULL,'2017-11-24 10:48:33'),(186,20,5,NULL,'2017-11-27 12:10:49'),(187,21,5,NULL,'2017-11-29 15:15:39'),(188,22,5,NULL,'2017-12-01 15:08:22'),(189,23,5,NULL,'2017-12-04 10:27:48'),(190,24,5,NULL,'2017-12-06 08:34:52'),(191,25,3,NULL,'2017-12-06 16:01:36'),(192,25,5,NULL,'2017-12-06 17:19:11'),(193,26,5,NULL,'2017-12-08 11:35:44'),(194,26,1,'2017-12-08 12:29:43','2017-12-08 12:27:57'),(195,27,5,NULL,'2017-12-12 11:40:34'),(196,28,5,NULL,'2017-12-13 16:41:26');

/*Table structure for table `tb_categories` */

DROP TABLE IF EXISTS `tb_categories`;

CREATE TABLE `tb_categories` (
  `idcategory` int(11) NOT NULL AUTO_INCREMENT,
  `descategory` varchar(32) NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcategory`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `tb_categories` */

insert  into `tb_categories`(`idcategory`,`descategory`,`dtregister`) values (1,'Android','2017-09-26 16:02:59'),(2,'Apple','2017-09-26 16:03:09'),(3,'Motorola','2017-09-26 16:03:18'),(4,'Samsung','2017-09-26 16:03:28');

/*Table structure for table `tb_orders` */

DROP TABLE IF EXISTS `tb_orders`;

CREATE TABLE `tb_orders` (
  `idorder` int(11) NOT NULL AUTO_INCREMENT,
  `idcart` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idaddress` int(11) NOT NULL,
  `idstatus` int(11) NOT NULL,
  `vltotal` decimal(10,2) NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idorder`),
  KEY `FK_orders_carts_idx` (`idcart`),
  KEY `FK_orders_users_idx` (`iduser`),
  KEY `fk_orders_ordersstatus_idx` (`idstatus`),
  CONSTRAINT `fk_orders_carts` FOREIGN KEY (`idcart`) REFERENCES `tb_carts` (`idcart`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_ordersstatus` FOREIGN KEY (`idstatus`) REFERENCES `tb_ordersstatus` (`idstatus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_users` FOREIGN KEY (`iduser`) REFERENCES `tb_users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

/*Data for the table `tb_orders` */

insert  into `tb_orders`(`idorder`,`idcart`,`iduser`,`idaddress`,`idstatus`,`vltotal`,`dtregister`) values (3,9,1,13,1,'61.82','2017-10-06 16:43:41'),(4,10,1,14,1,'61.82','2017-10-09 10:04:41'),(5,10,1,15,1,'4678.31','2017-10-09 10:08:59'),(6,10,1,1,3,'7016.71','2017-10-09 14:13:44'),(7,12,1,1,1,'716.35','2017-10-11 16:12:27'),(8,14,10,2,1,'1192.49','2017-11-21 13:57:28'),(9,14,10,3,1,'1192.49','2017-11-21 14:00:38'),(10,14,10,4,1,'2359.82','2017-11-21 16:04:41'),(11,14,10,5,1,'3297.95','2017-11-21 16:13:16'),(12,14,10,6,1,'3297.95','2017-11-21 16:18:29'),(13,14,10,7,1,'1344.74','2017-11-21 16:19:05'),(14,14,10,8,1,'1344.74','2017-11-21 16:22:15'),(15,14,10,9,1,'1344.74','2017-11-21 16:25:28'),(16,14,10,10,1,'1344.74','2017-11-21 16:43:20'),(17,14,10,11,1,'1344.74','2017-11-21 16:45:14'),(18,15,10,12,1,'1192.49','2017-11-21 16:47:11'),(19,15,10,13,1,'1192.49','2017-11-21 16:49:02'),(20,15,10,14,1,'1192.49','2017-11-21 16:57:23'),(21,15,10,15,1,'1192.49','2017-11-21 16:58:13'),(22,16,10,16,1,'0.00','2017-11-21 16:59:04'),(23,14,10,17,1,'1344.74','2017-11-21 17:07:33'),(24,14,10,18,1,'1344.74','2017-11-21 17:10:14'),(25,14,10,19,1,'1344.74','2017-11-21 17:23:56'),(26,17,10,20,1,'1149.00','2017-11-21 20:46:14'),(27,18,10,21,1,'1192.49','2017-11-22 08:18:33'),(28,18,10,22,1,'1192.49','2017-11-22 11:23:24'),(29,18,10,23,1,'1192.49','2017-11-22 11:25:58'),(30,18,10,24,1,'1192.49','2017-11-22 11:36:03'),(31,19,10,25,1,'1192.49','2017-11-24 10:32:39'),(32,19,10,26,1,'2359.82','2017-11-24 10:49:11'),(33,19,10,27,1,'2359.82','2017-11-24 11:58:13'),(34,19,10,28,1,'2359.82','2017-11-24 12:15:35'),(35,20,10,29,1,'1192.49','2017-11-27 12:11:09'),(36,20,10,30,1,'1192.49','2017-11-27 12:11:27'),(37,20,10,31,1,'1192.49','2017-11-27 12:44:55'),(38,20,10,32,1,'1192.49','2017-11-27 12:52:55'),(39,21,10,33,1,'1192.49','2017-11-29 15:15:54'),(40,21,10,34,1,'1192.49','2017-11-29 16:38:05'),(41,21,10,35,1,'1192.49','2017-11-29 16:41:43'),(42,21,10,36,1,'1192.49','2017-11-29 16:57:50'),(43,22,10,37,1,'1192.49','2017-12-01 15:08:56'),(44,23,10,38,1,'1192.49','2017-12-04 10:28:17'),(45,24,10,39,1,'1192.49','2017-12-06 08:35:11'),(46,24,1,40,1,'1192.49','2017-12-06 12:54:23'),(47,24,1,41,1,'1192.49','2017-12-06 13:01:38'),(48,25,10,42,1,'1942.35','2017-12-06 16:03:05'),(49,25,10,43,1,'1942.35','2017-12-06 16:25:25'),(50,25,10,44,1,'3108.58','2017-12-06 17:19:25'),(51,26,10,45,1,'1192.49','2017-12-08 11:36:21'),(52,26,10,46,1,'1192.49','2017-12-08 12:46:09'),(53,27,10,47,1,'1192.49','2017-12-12 11:40:55'),(54,27,10,48,1,'1192.49','2017-12-12 11:54:20'),(55,27,10,49,1,'1192.49','2017-12-12 15:28:34'),(56,27,10,50,1,'1192.49','2017-12-12 15:31:12'),(57,28,10,51,1,'1192.49','2017-12-13 16:41:42');

/*Table structure for table `tb_orderspagseguro` */

DROP TABLE IF EXISTS `tb_orderspagseguro`;

CREATE TABLE `tb_orderspagseguro` (
  `idorder` int(11) NOT NULL,
  `descode` varchar(36) NOT NULL,
  `vlgrossamount` decimal(10,2) NOT NULL,
  `vldiscountamount` decimal(10,2) NOT NULL,
  `vlfeeamount` decimal(10,2) NOT NULL,
  `vlnetamount` decimal(10,2) NOT NULL,
  `vlextraamount` decimal(10,2) NOT NULL,
  `despaymentlink` varchar(256) DEFAULT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idorder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_orderspagseguro` */

insert  into `tb_orderspagseguro`(`idorder`,`descode`,`vlgrossamount`,`vldiscountamount`,`vlfeeamount`,`vlnetamount`,`vlextraamount`,`despaymentlink`,`dtregister`) values (45,'0','1192.49','0.00','183.69','1008.69','0.00','','2017-12-06 12:43:32'),(46,'8.0E+179','1192.49','0.00','132.05','1060.44','0.00','','2017-12-06 13:00:28'),(47,'0','1192.49','0.00','132.05','1060.44','0.00','','2017-12-06 13:02:59'),(51,'EAA98841-996B-45BC-8C39-8F79D8B9A6D0','1192.49','0.00','104.98','1087.51','0.00','','2017-12-08 12:27:40'),(52,'48EAF126-5B1A-4CD9-82CF-5510F1CD2D89','1192.49','0.00','118.69','1073.80','0.00','','2017-12-08 12:47:39'),(53,'B028FAD9-CB2D-4989-80A5-067E79BBB973','1192.49','0.00','118.69','1073.80','0.00','','2017-12-12 11:50:07'),(54,'D6CEC6BC-58C1-473F-A845-D10BC211993A','1192.49','0.00','47.98','1144.51','0.00','https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=f8bbebfed765ad67327806f263f736589ca32ceaafaf46faf605767e3b1b44c0834ecb8d464d27d0','2017-12-12 12:02:47'),(55,'DF329105-4C6B-4772-8581-756328A38295','1192.49','0.00','47.98','1144.51','0.00','https://sandbox.pagseguro.uol.com.br/checkout/payment/eft/print.jhtml?c=0b270ad646df679daa84bf712e4f840b5477e79fbeb5ef63759c214d91a71e757a9db7e9e6ba9ab5','2017-12-12 15:29:47'),(56,'FB82B8C6-3D3E-468C-BEC7-E67FC3FD6113','1192.49','0.00','47.98','1144.51','0.00','https://sandbox.pagseguro.uol.com.br/checkout/payment/eft/print.jhtml?c=2f4ebc6cbb64a170ebf37ee143a549d9c31beed5682737c9af548b4cf374141d787d5f0e6c3ff791','2017-12-12 15:32:31');

/*Table structure for table `tb_ordersstatus` */

DROP TABLE IF EXISTS `tb_ordersstatus`;

CREATE TABLE `tb_ordersstatus` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `desstatus` varchar(32) NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `tb_ordersstatus` */

insert  into `tb_ordersstatus`(`idstatus`,`desstatus`,`dtregister`) values (1,'Em Aberto','2017-03-13 00:00:00'),(2,'Aguardando Pagamento','2017-03-13 00:00:00'),(3,'Pago','2017-03-13 00:00:00'),(4,'Entregue','2017-03-13 00:00:00');

/*Table structure for table `tb_persons` */

DROP TABLE IF EXISTS `tb_persons`;

CREATE TABLE `tb_persons` (
  `idperson` int(11) NOT NULL AUTO_INCREMENT,
  `desperson` varchar(64) NOT NULL,
  `desemail` varchar(128) DEFAULT NULL,
  `nrphone` bigint(20) DEFAULT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idperson`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `tb_persons` */

insert  into `tb_persons`(`idperson`,`desperson`,`desemail`,`nrphone`,`dtregister`) values (1,'Arthur Alves','arthur.alvesdeveloper@sandbox.pagseguro.com.br',2147483647,'2017-03-01 00:00:00'),(7,'Suporte','arthur.alvesdeveloper@sandbox.pagseguro.com.br',1112345678,'2017-03-15 13:10:27'),(8,'Arthur Alves','arthur.alvesdeveloper@sandbox.pagseguro.com.br',11948667863,'2017-09-27 09:06:33'),(10,'Arthur Alves','arthur.alvesdeveloper@sandbox.pagseguro.com.br',11940463545,'2017-09-28 10:32:45'),(12,'cliente','arthur.alvesdeveloper@sandbox.pagseguro.com.br',12321321,'2017-09-29 16:06:13'),(15,'Arthur Alves','arthur.alvesdeveloper@sandbox.pagseguro.com.br',2147483647,'2017-10-02 16:15:12'),(16,'Arthur Alves','arthur.alvesdeveloper@sandbox.pagseguro.com.br',11940463545,'2017-11-29 16:36:04'),(17,'Arthur Alves','arthur.alvesdeveloper@sandbox.pagseguro.com.br',11940463545,'2017-11-29 16:39:48');

/*Table structure for table `tb_products` */

DROP TABLE IF EXISTS `tb_products`;

CREATE TABLE `tb_products` (
  `idproduct` int(11) NOT NULL AUTO_INCREMENT,
  `desproduct` varchar(255) NOT NULL,
  `vlprice` decimal(10,2) NOT NULL,
  `vlwidth` decimal(10,2) NOT NULL,
  `vlheight` decimal(10,2) NOT NULL,
  `vllength` decimal(10,2) NOT NULL,
  `vlweight` decimal(10,2) NOT NULL,
  `desurl` varchar(128) NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idproduct`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `tb_products` */

insert  into `tb_products`(`idproduct`,`desproduct`,`vlprice`,`vlwidth`,`vlheight`,`vllength`,`vlweight`,`desurl`,`dtregister`) values (1,'Motorola Moto G5 Plus','999.95','75.00','151.00','80.00','167.00','smartphone-android-7.0','2017-09-27 09:07:18'),(2,'Smartphone Moto Z Play','1135.23','15.20','7.40','0.70','0.16','smartphone-motorola-moto-g5-plus','2017-09-27 09:08:18'),(3,'Smartphone Samsung Galaxy J5 Pro','1887.78','14.10','0.90','1.16','0.13','smartphone-moto-z-play','2017-09-27 09:10:41'),(4,'Smartphone Samsung Galaxy J3 Pro','1299.00','14.60','7.10','0.80','0.16','smartphone-samsung-galaxy-j5','2017-09-27 09:11:21'),(5,'iPad Apple 128GB Prata Tela 9,7â€ Retina - Proc. Chip A9 CÃ¢m. 8MP + Frontal iOS 10 Touch ID','1149.00','15.10','7.50','0.80','0.16','smartphone-samsung-galaxy-j7','2017-09-27 09:11:54'),(6,'Smartphone Samsung Galaxy J3 Dual','679.90','14.20','7.10','0.70','0.14','smartphone-samsung-galaxy-j3','2017-09-27 09:12:24');

/*Table structure for table `tb_productscategories` */

DROP TABLE IF EXISTS `tb_productscategories`;

CREATE TABLE `tb_productscategories` (
  `idcategory` int(11) NOT NULL,
  `idproduct` int(11) NOT NULL,
  PRIMARY KEY (`idcategory`,`idproduct`),
  KEY `fk_productscategories_products_idx` (`idproduct`),
  CONSTRAINT `fk_productscategories_categories` FOREIGN KEY (`idcategory`) REFERENCES `tb_categories` (`idcategory`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productscategories_products` FOREIGN KEY (`idproduct`) REFERENCES `tb_products` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_productscategories` */

insert  into `tb_productscategories`(`idcategory`,`idproduct`) values (3,1),(3,2),(4,3),(4,4),(2,5),(4,6);

/*Table structure for table `tb_users` */

DROP TABLE IF EXISTS `tb_users`;

CREATE TABLE `tb_users` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `idperson` int(11) NOT NULL,
  `deslogin` varchar(64) NOT NULL,
  `despassword` varchar(256) NOT NULL,
  `inadmin` tinyint(4) NOT NULL DEFAULT '0',
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iduser`),
  KEY `FK_users_persons_idx` (`idperson`),
  CONSTRAINT `fk_users_persons` FOREIGN KEY (`idperson`) REFERENCES `tb_persons` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `tb_users` */

insert  into `tb_users`(`iduser`,`idperson`,`deslogin`,`despassword`,`inadmin`,`dtregister`) values (1,1,'admin','$2y$12$9HoyJLDJgZLwyuUQtThXsulBaE9M.ieBa.GpDKudE6b7HG0xnLiHW',1,'2017-03-13 00:00:00'),(7,7,'suporte','$2y$12$HFjgUm/mk1RzTy4ZkJaZBe0Mc/BA2hQyoUckvm.lFa6TesjtNpiMe',1,'2017-03-15 13:10:27'),(8,8,'arthur.alves','$2y$12$YlooCyNvyTji8bPRcrfNfOKnVMmZA9ViM2A3IpFjmrpIbp5ovNmga',1,'2017-09-27 09:06:33'),(10,10,'xartx157','$2y$12$OxL8eQ3cylQgiXSYS8m6nuhyoBUMW1KWZ6B6cr5wycQhvexhfE9TK',1,'2017-09-28 10:32:45'),(12,12,'blackstaroficial@sandbox.pagseguro.com.br','$2y$12$cHmJvWIbNG8uQKQEkxTnYOPyywfuzoT7V7qag/kmqU07dAOp2akBS',0,'2017-09-29 16:06:13'),(15,15,'admin@hcode.com.br','$2y$12$ZcSasupDK.lxWALxfxr6qe.WiS8peBcJQsb2.u8XLdycaJ82Om.HS',1,'2017-10-02 16:15:12'),(16,16,'blackstaroficial@sandbox.pagseguro.com.br','$2y$12$CCaSlEJ6L9lxOhRzMypA5.dw5vZd79zhwxrxmsZN5bBIDtc.8/kRO',1,'2017-11-29 16:36:04'),(17,17,'blackstaroficial@sandbox.pagseguro.com.br','$2y$12$9/gN2KVCBx1DGYckdbnr/OG8ze57iLSf..6lnLGSO6pKSxF8bGiF.',1,'2017-11-29 16:39:49');

/*Table structure for table `tb_userslogs` */

DROP TABLE IF EXISTS `tb_userslogs`;

CREATE TABLE `tb_userslogs` (
  `idlog` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `deslog` varchar(128) NOT NULL,
  `desip` varchar(45) NOT NULL,
  `desuseragent` varchar(128) NOT NULL,
  `dessessionid` varchar(64) NOT NULL,
  `desurl` varchar(128) NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idlog`),
  KEY `fk_userslogs_users_idx` (`iduser`),
  CONSTRAINT `fk_userslogs_users` FOREIGN KEY (`iduser`) REFERENCES `tb_users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_userslogs` */

/*Table structure for table `tb_userspasswordsrecoveries` */

DROP TABLE IF EXISTS `tb_userspasswordsrecoveries`;

CREATE TABLE `tb_userspasswordsrecoveries` (
  `idrecovery` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `desip` varchar(45) NOT NULL,
  `dtrecovery` datetime DEFAULT NULL,
  `dtregister` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idrecovery`),
  KEY `fk_userspasswordsrecoveries_users_idx` (`iduser`),
  CONSTRAINT `fk_userspasswordsrecoveries_users` FOREIGN KEY (`iduser`) REFERENCES `tb_users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `tb_userspasswordsrecoveries` */

insert  into `tb_userspasswordsrecoveries`(`idrecovery`,`iduser`,`desip`,`dtrecovery`,`dtregister`) values (1,7,'127.0.0.1',NULL,'2017-03-15 13:10:59'),(2,7,'127.0.0.1','2017-03-15 13:33:45','2017-03-15 13:11:18'),(3,7,'127.0.0.1','2017-03-15 13:37:35','2017-03-15 13:37:12'),(4,10,'127.0.0.1',NULL,'2017-10-02 10:46:20'),(5,10,'127.0.0.1',NULL,'2017-10-02 10:51:25'),(6,10,'127.0.0.1',NULL,'2017-10-02 10:52:27'),(7,10,'127.0.0.1',NULL,'2017-10-02 10:54:22'),(8,10,'127.0.0.1',NULL,'2017-10-02 10:54:23'),(9,10,'127.0.0.1',NULL,'2017-10-02 10:55:33'),(10,10,'127.0.0.1',NULL,'2017-10-02 10:56:25'),(11,10,'127.0.0.1',NULL,'2017-10-02 10:58:37'),(12,10,'127.0.0.1',NULL,'2017-10-02 11:00:35'),(13,10,'127.0.0.1',NULL,'2017-10-02 11:06:13'),(14,10,'127.0.0.1',NULL,'2017-10-02 11:06:48');

/* Procedure structure for procedure `sp_addresses_save` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_addresses_save` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_addresses_save`(
pidaddress int(11), 
pidperson int(11),
pdesaddress varchar(128),
pdesnumber varchar(16),
pdescomplement varchar(32),
pdescity varchar(32),
pdesstate varchar(32),
pdescountry varchar(32),
pdeszipcode char(8),
pdesdistrict varchar(32)
)
BEGIN

	IF pidaddress > 0 THEN
		
		UPDATE tb_addresses
        SET
			idperson = pidperson,
            desaddress = pdesaddress,
            desnumber = pdesnumber,
            descomplement = pdescomplement,
            descity = pdescity,
            desstate = pdesstate,
            descountry = pdescountry,
            deszipcode = pdeszipcode, 
            desdistrict = pdesdistrict
		WHERE idaddress = pidaddress;
        
    ELSE
		
		INSERT INTO tb_addresses (idperson, desaddress, desnumber, descomplement, descity, desstate, descountry, deszipcode, desdistrict)
        VALUES(pidperson, pdesaddress, pdesnumber, pdescomplement, pdescity, pdesstate, pdescountry, pdeszipcode, pdesdistrict);
        
        SET pidaddress = LAST_INSERT_ID();
        
    END IF;
    
    SELECT * FROM tb_addresses WHERE idaddress = pidaddress;

END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_carts_save` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_carts_save` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_carts_save`(
pidcart INT,
pdessessionid VARCHAR(64),
piduser INT,
pdeszipcode CHAR(8),
pvlfreight DECIMAL(10,2),
pnrdays INT
)
BEGIN
    IF pidcart > 0 THEN
        
        UPDATE tb_carts
        SET
            dessessionid = pdessessionid,
            iduser = piduser,
            deszipcode = pdeszipcode,
            vlfreight = pvlfreight,
            nrdays = pnrdays
        WHERE idcart = pidcart;
        
    ELSE
        
        INSERT INTO tb_carts (dessessionid, iduser, deszipcode, vlfreight, nrdays)
        VALUES(pdessessionid, piduser, pdeszipcode, pvlfreight, pnrdays);
        
        SET pidcart = LAST_INSERT_ID();
        
    END IF;
    
    SELECT * FROM tb_carts WHERE idcart = pidcart;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_categories_save` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_categories_save` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_categories_save`(
pidcategory INT,
pdescategory VARCHAR(64)
)
BEGIN
	
	IF pidcategory > 0 THEN
		
		UPDATE tb_categories
        SET descategory = pdescategory
        WHERE idcategory = pidcategory;
        
    ELSE
		
		INSERT INTO tb_categories (descategory) VALUES(pdescategory);
        
        SET pidcategory = LAST_INSERT_ID();
        
    END IF;
    
    SELECT * FROM tb_categories WHERE idcategory = pidcategory;
    
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_orderspagseguro_save` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_orderspagseguro_save` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_orderspagseguro_save`(
pidorder int, 
pdescode varchar(36), 
pvlgrossamount decimal(10,2), 
pvldiscountamount decimal(10,2), 
pvlfeeamount decimal(10,2), 
pvlnetamount decimal(10,2), 
pvlextraamount decimal(10,2),
pdespaymentlink varchar(256)
)
BEGIN
	
    DELETE FROM tb_orderspagseguro WHERE idorder = pidorder;
    
    INSERT INTO tb_orderspagseguro (idorder, descode, vlgrossamount, vldiscountamount, vlfeeamount, vlnetamount, vlextraamount, despaymentlink)
	VALUES(pidorder, pdescode, pvlgrossamount, pvldiscountamount, pvlfeeamount, pvlnetamount, pvlextraamount, pdespaymentlink);
    
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_orders_save` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_orders_save` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_orders_save`(
pidorder INT,
pidcart int(11),
piduser int(11),
pidstatus int(11),
pidaddress int(11),
pvltotal decimal(10,2)
)
BEGIN
	
	IF pidorder > 0 THEN
		
		UPDATE tb_orders
        SET
			idcart = pidcart,
            iduser = piduser,
            idstatus = pidstatus,
            idaddress = pidaddress,
            vltotal = pvltotal
		WHERE idorder = pidorder;
        
    ELSE
    
		INSERT INTO tb_orders (idcart, iduser, idstatus, idaddress, vltotal)
        VALUES(pidcart, piduser, pidstatus, pidaddress, pvltotal);
		
		SET pidorder = LAST_INSERT_ID();
        
    END IF;
    
    SELECT * 
    FROM tb_orders a
    INNER JOIN tb_ordersstatus b USING(idstatus)
    INNER JOIN tb_carts c USING(idcart)
    INNER JOIN tb_users d ON d.iduser = a.iduser
    INNER JOIN tb_addresses e USING(idaddress)
    WHERE idorder = pidorder;
    
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_products_save` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_products_save` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_products_save`(
pidproduct int(11),
pdesproduct varchar(255),
pvlprice decimal(10,2),
pvlwidth decimal(10,2),
pvlheight decimal(10,2),
pvllength decimal(10,2),
pvlweight decimal(10,2),
pdesurl varchar(128)
)
BEGIN
	
	IF pidproduct > 0 THEN
		
		UPDATE tb_products
        SET 
			desproduct = pdesproduct,
            vlprice = pvlprice,
            vlwidth = pvlwidth,
            vlheight = pvlheight,
            vllength = pvllength,
            vlweight = pvlweight,
            desurl = pdesurl
        WHERE idproduct = pidproduct;
        
    ELSE
		
		INSERT INTO tb_products (desproduct, vlprice, vlwidth, vlheight, vllength, vlweight, desurl) 
        VALUES(pdesproduct, pvlprice, pvlwidth, pvlheight, pvllength, pvlweight, pdesurl);
        
        SET pidproduct = LAST_INSERT_ID();
        
    END IF;
    
    SELECT * FROM tb_products WHERE idproduct = pidproduct;
    
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_userspasswordsrecoveries_create` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_userspasswordsrecoveries_create` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_userspasswordsrecoveries_create`(
piduser INT,
pdesip VARCHAR(45)
)
BEGIN
	
	INSERT INTO tb_userspasswordsrecoveries (iduser, desip)
    VALUES(piduser, pdesip);
    
    SELECT * FROM tb_userspasswordsrecoveries
    WHERE idrecovery = LAST_INSERT_ID();
    
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_usersupdate_save` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_usersupdate_save` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_usersupdate_save`(
piduser INT,
pdesperson VARCHAR(64), 
pdeslogin VARCHAR(64), 
pdespassword VARCHAR(256), 
pdesemail VARCHAR(128), 
pnrphone BIGINT, 
pinadmin TINYINT
)
BEGIN
	
    DECLARE vidperson INT;
    
	SELECT idperson INTO vidperson
    FROM tb_users
    WHERE iduser = piduser;
    
    UPDATE tb_persons
    SET 
		desperson = pdesperson,
        desemail = pdesemail,
        nrphone = pnrphone
	WHERE idperson = vidperson;
    
    UPDATE tb_users
    SET
		deslogin = pdeslogin,
        despassword = pdespassword,
        inadmin = pinadmin
	WHERE iduser = piduser;
    
    SELECT * FROM tb_users a INNER JOIN tb_persons b USING(idperson) WHERE a.iduser = piduser;
    
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_users_delete` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_users_delete` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_delete`(
piduser INT
)
BEGIN
    
    DECLARE vidperson INT;
    
    SET FOREIGN_KEY_CHECKS = 0;
	
	SELECT idperson INTO vidperson
    FROM tb_users
    WHERE iduser = piduser;
	
    DELETE FROM tb_addresses WHERE idperson = vidperson;
    DELETE FROM tb_addresses WHERE idaddress IN(SELECT idaddress FROM tb_orders WHERE iduser = piduser);
	DELETE FROM tb_persons WHERE idperson = vidperson;
    
    DELETE FROM tb_userslogs WHERE iduser = piduser;
    DELETE FROM tb_userspasswordsrecoveries WHERE iduser = piduser;
    DELETE FROM tb_orders WHERE iduser = piduser;
    DELETE FROM tb_cartsproducts WHERE idcart IN(SELECT idcart FROM tb_carts WHERE iduser = piduser);
    DELETE FROM tb_carts WHERE iduser = piduser;
    DELETE FROM tb_users WHERE iduser = piduser;
    
    SET FOREIGN_KEY_CHECKS = 1;
    
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_users_save` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_users_save` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_save`(
pdesperson VARCHAR(64), 
pdeslogin VARCHAR(64), 
pdespassword VARCHAR(256), 
pdesemail VARCHAR(128), 
pnrphone BIGINT, 
pinadmin TINYINT
)
BEGIN
	
    DECLARE vidperson INT;
    
	INSERT INTO tb_persons (desperson, desemail, nrphone)
    VALUES(pdesperson, pdesemail, pnrphone);
    
    SET vidperson = LAST_INSERT_ID();
    
    INSERT INTO tb_users (idperson, deslogin, despassword, inadmin)
    VALUES(vidperson, pdeslogin, pdespassword, pinadmin);
    
    SELECT * FROM tb_users a INNER JOIN tb_persons b USING(idperson) WHERE a.iduser = LAST_INSERT_ID();
    
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
